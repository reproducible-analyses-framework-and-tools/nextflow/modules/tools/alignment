// Preprocessing
include { manifest_to_raw_fqs } from '../preproc/preproc.nf'
include { raw_fqs_to_procd_fqs } from '../preproc/preproc.nf'

// STAR
include { star_index } from '../star/star.nf'
include { star_map } from '../star/star.nf'

// BWA
include { bwa_index } from '../bwa/bwa.nf'
include { bwa_mem } from '../bwa/bwa.nf'
include { bwa_mem_samtools_sort } from '../bwa/bwa.nf'

//BWA-MEM2
include { bwa_mem2_index } from '../bwa_mem2/bwa_mem2.nf'
include { bwa_mem2 } from '../bwa_mem2/bwa_mem2.nf'
include { bwa_mem2_mem_samtools_sort } from '../bwa_mem2/bwa_mem2.nf'

// bbmap
include { bbmap_index } from '../bbmap/bbmap.nf'
include { bbmap } from '../bbmap/bbmap.nf'
include { bbmap_samtools_sort } from '../bbmap/bbmap.nf'
include { bbmap_index as bbmap_index_alt_ref } from '../bbmap/bbmap.nf'
include { bbmap as bbmap_alt_ref } from '../bbmap/bbmap.nf'
include { bbmap_samtools_sort as bbmap_samtools_sort_alt_ref } from '../bbmap/bbmap.nf'

// minimap2
include { minimap2_samtools_sort } from '../minimap2/minimap2.nf'

// mm2-fast
include { mm2_fast_samtools_sort } from '../mm2-fast/mm2-fast.nf'

// NGMLR
include { ngmlr } from '../ngmlr/ngmlr.nf'

// mapquik
include { mapquik } from '../mapquik/mapquik.nf'

// hisat2
include { hisat2_build } from '../hisat2/hisat2.nf'
include { hisat2 } from '../hisat2/hisat2.nf'
include { hisat2_samtools_sort } from '../hisat2/hisat2.nf'

// bowtie2
include { bowtie2_build } from '../bowtie2/bowtie2.nf'
include { bowtie2 } from '../bowtie2/bowtie2.nf'
include { bowtie2_samtools_sort } from '../bowtie2/bowtie2.nf'

// Misc
include { samtools_faidx } from '../samtools/samtools.nf'
include { samtools_index as samtools_index_procd} from '../samtools/samtools.nf'
include { samtools_index } from '../samtools/samtools.nf'
include { picard_create_seq_dict } from '../picard2/picard2.nf'
include { gatk_index_feature_file } from '../gatk4/gatk4.nf'

// Duplicate marking or removal
include { picard_mark_duplicates } from '../picard2/picard2.nf'
include { bamblaster } from '../samblaster/samblaster.nf'

// Base quality recalibration
include { samtools_index as samtools_index_recal } from '../samtools/samtools.nf'
include { samtools_index as samtools_index_bqsr } from '../samtools/samtools.nf'
include { gatk_base_recalibrator } from '../gatk4/gatk4.nf'
include { gatk_apply_bqsr } from '../gatk4/gatk4.nf'

// InDel realignment
include { bams_bais_to_realigned_bams } from '../abra2/abra2.nf'
include { abra2_rna } from '../abra2/abra2.nf'

// Intermediate file cleaning
include { clean_work_files as clean_trimmed_fastqs } from '../utilities/utilities.nf'
include { clean_work_files as clean_init_bams } from '../utilities/utilities.nf'
include { clean_work_files as clean_init_bams2 } from '../utilities/utilities.nf'
include { clean_work_files as clean_init_bams3 } from '../utilities/utilities.nf'
include { clean_work_files as clean_init_bams4 } from '../utilities/utilities.nf'

workflow manifest_to_alns {
// require:
//   MANIFEST
//   params.alignment$manifest_to_alns$fq_trim_tool
//   params.alignment$manifest_to_alns$fq_trim_tool_parameters
//   params.alignment$manifest_to_alns$aln_tool
//   params.alignment$manifest_to_alns$aln_tool_parameters
//   params.alignment$manifest_to_alns$aln_ref
//   params.alignment$manifest_to_alns$gtf
//   params.alignment$manifest_to_alns$alt_ref
  take:
    manifest
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    aln_ref
    gtf
    alt_ref
  main:
    manifest_to_raw_fqs(
      manifest)
    raw_fqs_to_alns(
      manifest_to_raw_fqs.out.fqs,
      fq_trim_tool,
      fq_trim_tool_parameters,
      aln_tool,
      aln_tool_parameters,
      aln_ref,
      gtf,
      alt_ref)
  emit:
    fqs = manifest_to_raw_fqs.out.fqs
    procd_fqs = raw_fqs_to_alns.out.procd_fqs
    alns = raw_fqs_to_alns.out.alns
    alt_alns = raw_fqs_to_alns.out.alt_alns
    junctions = raw_fqs_to_alns.out.junctions
}


workflow raw_fqs_to_alns {
// require:
//   FQS
//   params.alignment$raw_fqs_to_alns$fq_trim_tool
//   params.alignment$raw_fqs_to_alns$fq_trim_tool_parameters
//   params.alignment$raw_fqs_to_alns$aln_tool
//   params.alignment$raw_fqs_to_alns$aln_tool_parameters
//   params.alignment$raw_fqs_to_alns$aln_ref
//   params.alignment$raw_fqs_to_alns$gtf
//   params.alignment$raw_fqs_to_alns$alt_ref
  take:
    fqs
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    aln_ref
    gtf
    alt_ref
  main:
    raw_fqs_to_procd_fqs(
      fqs,
      fq_trim_tool,
      fq_trim_tool_parameters)
    procd_fqs_to_alns(
      raw_fqs_to_procd_fqs.out.procd_fqs,
      aln_tool,
      aln_tool_parameters,
      aln_ref,
      gtf,
      alt_ref)
  emit:
    procd_fqs = raw_fqs_to_procd_fqs.out.procd_fqs
    alns = procd_fqs_to_alns.out.alns
    alt_alns = procd_fqs_to_alns.out.alt_alns
    junctions = procd_fqs_to_alns.out.junctions
}


workflow procd_fqs_to_alns {
// require:
//   PROCD_FQS
//   params.alignment$procd_fqs_to_alns$aln_tool
//   params.alignment$procd_fqs_to_alns$aln_tool_parameters
//   params.alignment$procd_fqs_to_alns$aln_ref
//   params.alignment$procd_fqs_to_alns$gtf
//   params.alignment$procd_fqs_to_alns$alt_ref
  take:
    procd_fqs
    aln_tool
    aln_tool_parameters
    aln_ref
    gtf
    alt_ref
  main:
    alns = ''
    alt_alns = ''
    junctions = ''
    aln_tool_parameters = Eval.me(aln_tool_parameters)
    if( aln_tool =~ /bbmap/ ) {
      bbmap_index_parameters = aln_tool_parameters['bbmap_index'] ? aln_tool_parameters['bbmap_index'] : ''
      bbmap_parameters = aln_tool_parameters['bbmap'] ? aln_tool_parameters['bbmap'] : ''
      bbmap_index_alt_ref_parameters = aln_tool_parameters['bbmap_index_alt_ref'] ? aln_tool_parameters['bbmap_index_alt_ref'] : ''
      bbmap_alt_ref_parameters = aln_tool_parameters['bbmap_alt_ref'] ? aln_tool_parameters['bbmap_alt_ref'] : ''
      bbmap_index(
        aln_ref,
        bbmap_index_parameters)
      bbmap_samtools_sort(
        procd_fqs,
        bbmap_index.out.idx_files,
        bbmap_parameters)
      bbmap_samtools_sort.out.alns
        .set{ alns }
      // This is support the ability for bbmap to align to both a reference
      // genome FASTA and a reference transcriptome FASTA.
      if( alt_ref =~ /[A-Z][a-z]/ ) {
        bbmap_index_alt_ref(
          alt_ref,
          bbmap_index_alt_ref_parameters)
        bbmap_samtools_sort_alt_ref(
          procd_fqs,
          bbmap_index_alt_ref.out.idx_files,
          bbmap_alt_ref_parameters)
        bbmap_samtools_sort.out.alns
          .set{ alt_alns }
      }
    }
    if( aln_tool =~ /bwa$/ ) {
      bwa_index_parameters = aln_tool_parameters['bwa_index'] ? aln_tool_parameters['bwa_index'] : ''
      bwa_parameters = aln_tool_parameters['bwa'] ? aln_tool_parameters['bwa'] : ''
      bwa_index(
        aln_ref,
        bwa_index_parameters)
      bwa_mem_samtools_sort(
        procd_fqs,
        bwa_index.out.idx_files,
        bwa_parameters)
      bwa_mem_samtools_sort.out.bams
        .set{ alns }
    }
    if( aln_tool =~ /bwa-mem2/ ) {
      bwa_mem2_index_parameters = aln_tool_parameters['bwa_mem2_index'] ? aln_tool_parameters['bwa_mem2_index'] : ''
      bwa_mem2_parameters = aln_tool_parameters['bwa_mem2'] ? aln_tool_parameters['bwa_mem2'] : ''
      bwa_mem2_index(
        aln_ref,
        bwa_mem2_index_parameters)
      bwa_mem2_mem_samtools_sort(
        procd_fqs,
        bwa_mem2_index.out.idx_files,
        bwa_mem2_parameters)
      bwa_mem2_mem_samtools_sort.out.bams
        .set{ alns }
    }
    if( aln_tool =~ /star/ ) {
      star_index_parameters = aln_tool_parameters['star_index'] ? aln_tool_parameters['star_index'] : ''
      star_map_parameters = aln_tool_parameters['star'] ? aln_tool_parameters['star'] : ''
      star_index(
        aln_ref,
        star_index_parameters)
      star_map(
        procd_fqs,
        star_index.out.idx_files,
        star_map_parameters,
        gtf)
      star_map.out.alns
        .set{ alns }
      star_map.out.alt_alns
        .set{ alt_alns }
      star_map.out.standard_junctions
        .set{ junctions }
    }
    if ( aln_tool =~ /minimap2/ ) {
      minimap2_parameters = aln_tool_parameters['minimap2'] ? aln_tool_parameters['minimap2'] : ''
      minimap2_samtools_sort(
        procd_fqs,
        aln_ref,
        minimap2_parameters)
      minimap2_samtools_sort.out.bams.set{ alns }
    }
    if ( aln_tool =~ /mm2-fast/ ) {
      mm2_fast_parameters = aln_tool_parameters['mm2-fast'] ? aln_tool_parameters['mm2-fast'] : ''
      mm2_fast_samtools_sort(
        procd_fqs,
        aln_ref,
        mm2_fast_parameters)
      mm2_fast_samtools_sort.out.bams.set{ alns }
    }
    if ( aln_tool =~ /ngmlr/ ) {
      ngmlr_parameters = aln_tool_parameters['ngmlr'] ? aln_tool_parameters['ngmlr'] : ''
      ngmlr(
        procd_fqs,
        aln_ref,
        ngmlr_parameters)
      ngmlr.out.bams.set{ alns }
    }
    if ( aln_tool =~ /mapquik/ ) {
      mapquik_parameters = aln_tool_parameters['mapquik'] ? aln_tool_parameters['mapquik'] : ''
      mapquik(
        procd_fqs,
        aln_ref,
        mapquik_parameters)
      mapquik.out.pafs.set{ alns }
    }
    if ( aln_tool =~ /hisat2/ ) {
      hisat2_build_parameters = aln_tool_parameters['hisat2_build'] ? aln_tool_parameters['hisat2_build'] : ''
      hisat2_parameters = aln_tool_parameters['hisat2'] ? aln_tool_parameters['hisat2'] : ''
      hisat2_build(
        aln_ref,
        hisat2_build_parameters)
      hisat2_samtools_sort(
        procd_fqs,
        hisat2_build.out.idx_files,
        hisat2_parameters)
      hisat2_samtools_sort.out.bams.set{ alns }
    }
    if ( aln_tool =~ /bowtie2/ ) {
      bowtie2_build_parameters = aln_tool_parameters['bowtie2_build'] ? aln_tool_parameters['bowtie2_build'] : ''
      bowtie2_parameters = aln_tool_parameters['bowtie2'] ? aln_tool_parameters['bowtie2'] : ''
      bowtie2_build(
        aln_ref,
        bowtie2_build_parameters)
      bowtie2_samtools_sort(
        procd_fqs,
        bowtie2_build.out.idx_files,
        bowtie2_parameters)
      bowtie2_samtools_sort.out.bams.set{ alns }
    }
    // Cleaning upstream input intermediates
//    procd_fqs
//      .concat(alns)
//      .groupTuple(by: [0, 1, 2], size: 2)
//      .flatten()
//      .filter{ it =~ /trimmed.fastq.gz$|trimmed.fq.gz$/ }
//      .set { procd_fqs_done_signal }
//    clean_trimmed_fastqs(
//      procd_fqs_done_signal)
emit:
  alns
  alt_alns
  junctions
}


workflow alns_to_procd_alns {
//Mimics GATK best practices. Can probably be improved.
// require:
//   ALNS
//   JUNCTIONS
//   VCFS
//   params.alignment$alns_to_procd_alns$aln_ref
//   params.alignment$alns_to_procd_alns$bed
//   params.alignment$alns_to_procd_alns$gtf
//   params.alignment$alns_to_procd_alns$dup_marker_tool
//   params.alignment$alns_to_procd_alns$dup_marker_tool_parameters
//   params.alignment$alns_to_procd_alns$base_recalibrator_tool
//   params.alignment$alns_to_procd_alns$base_recalibrator_tool_parameters
//   params.alignment$alns_to_procd_alns$indel_realign_tool
//   params.alignment$alns_to_procd_alns$indel_realign_tool_parameters
//   params.alignment$alns_to_procd_alns$known_sites_ref
//   MANIFEST
  take:
    alns
    junctions
    vcfs
    ref
    bed
    gtf
    dup_marker_tool
    dup_marker_tool_parameters
    base_recalibrator_tool
    base_recalibrator_tool_parameters
    indel_realign_tool
    indel_realign_tool_parameters
    known_sites_ref
    manifest
  main:
    procd_bams = ''
    procd_bais = ''
    procd_bams_and_bais = ''
    marked_dup_metrics = ''

    make_ancillary_index_files(
      ref)

    // InDel Realignment
    indel_realign_tool_parameters = Eval.me(indel_realign_tool_parameters)
    if( indel_realign_tool =~ /abra2($|,)/ ) {
      samtools_index(
        alns,
        '')
      abra2_parameters = indel_realign_tool_parameters['abra2'] ? indel_realign_tool_parameters['abra2'] : ''
      bams_bais_to_realigned_bams(
        ref,
        bed,
        abra2_parameters,
        alns,
        samtools_index.out.bais,
        manifest)
      bams_bais_to_realigned_bams.out.norm_tumor_abra_bams
        .map{ [it[0], it[1].split('-rel-')[0], it[2], it[3]] }
        .set{ trunc_norm_tumor_abra_bams }
      alns
        .join(trunc_norm_tumor_abra_bams, by: [0, 1, 2])
        .flatten()
        .filter{ it =~ /sorted.bam$/ }
        .set { init_bams_done_signal }
      clean_init_bams(
        init_bams_done_signal)
      bams_bais_to_realigned_bams.out.norm_tumor_abra_bams
        .set{ alns }
    }
    if( indel_realign_tool =~ /abra2_rna/ ) {
      abra2_rna_parameters = indel_realign_tool_parameters['abra2_rna'] ? indel_realign_tool_parameters['abra2_rna'] : ''
      samtools_index(
        alns,
        '')
      samtools_index.out.bams_and_bais
        .join(junctions, by: [0, 1, 2])
        .map{ [it[0], it[2], it[1], it[3], it[4], it[5]] }
        .join(vcfs.map{ [it[0], it[3], it[4]] }, by: [0, 1])
        .map{ [it[0], it[2], it[1], it[3], it[4], it[5], it[6]] }
        .set{ alns_juncts_vcf }
      alns_juncts_vcf
        .filter{ it[6] == null }
        .map{ [it[0], it[1], it[2], it[3], it[4], it[5], "${params.ref_dir}/dummy_file"] }
        .set{ alns_juncts_novcfs }
      alns_juncts_vcf
        .filter{ it[6] != null }
        .set{ alns_juncts_w_vcfs }
      alns_juncts_novcfs
        .concat(alns_juncts_w_vcfs)
        .set{ abra2_rna_inputs }
      abra2_rna(
        abra2_rna_inputs,
        ref,
        gtf,
        bed,
        abra2_rna_parameters,
        'tmp_dir')
      alns.map{ [it[0], it[2], it[1], it[3]] }
        .join(abra2_rna.out.abra_bams, by: [0, 1, 2])
        .flatten()
        .filter{ it =~ /out.bam$/ }
        .set { init_bams4_done_signal }
      clean_init_bams4(
        init_bams4_done_signal)
      abra2_rna.out.abra_bams
        .set{ alns }
    }

    // Duplicate marker
    dup_marker_tool_parameters = Eval.me(dup_marker_tool_parameters)
    if( dup_marker_tool =~ /picard|picard2/ ) {
      picard_mark_duplicates_parameters = dup_marker_tool_parameters['picard_mark_duplicates'] ? dup_marker_tool_parameters['picard_mark_duplicates'] : ''
      picard_mark_duplicates(
        alns,
        picard_mark_duplicates_parameters)

      alns
        .map{ [it[0], it[1].split('-rel-')[0], it[2], it[3]] }
        .set{ trunc_alns }

      picard_mark_duplicates.out.mkdup_bams
        .map{ [it[0], it[1].split('-rel-')[0], it[2], it[3]] }
        .set{ trunc_norm_tumor_picard_bams }

      trunc_alns
        .join(trunc_norm_tumor_picard_bams, by: [0, 1, 2])
        .flatten()
        .filter{ it =~ /abra.bam$|sorted.bam$/ }
        .set { init_bams_done_signal2 }
      clean_init_bams2(
        init_bams_done_signal2)

      picard_mark_duplicates.out.mkdup_bams
        .set{ alns }
      picard_mark_duplicates.out.marked_dup_metrics
        .set{ marked_dup_metrics }
    }

    if( dup_marker_tool =~ /bamblaster/ ) {
      bamblaster_parameters = dup_marker_tool_parameters['bamblaster'] ? dup_marker_tool_parameters['bamblaster'] : ''
      bamblaster(
        alns,
        bamblaster_parameters)

      alns
        .map{ [it[0], it[1].split('-rel-')[0], it[2], it[3]] }
        .set{ trunc_alns }

      bamblaster.out.bamblst_bams
        .map{ [it[0], it[1].split('-rel-')[0], it[2], it[3]] }
        .set{ trunc_norm_tumor_bamblaster_bams }

      trunc_alns
        .join(trunc_norm_tumor_bamblaster_bams, by: [0, 1, 2])
        .flatten()
        .filter{ it =~ /abra.bam$|sorted.bam$/ }
        .set { init_bams_done_signal2 }
      clean_init_bams2(
        init_bams_done_signal2)

      bamblaster.out.bamblst_bams
        .set{ alns }
    }
    // Base quality recalibration
    base_recalibrator_tool_parameters = Eval.me(base_recalibrator_tool_parameters)
    if( base_recalibrator_tool =~ /gatk|gatk4/ ) {
      gatk_index_feature_file_parameters = base_recalibrator_tool_parameters['gatk_index_feature_file'] ? base_recalibrator_tool_parameters['gatk_index_feature_file'] : ''
      gatk_base_recalibrator_parameters = base_recalibrator_tool_parameters['gatk_base_recalibrator'] ? base_recalibrator_tool_parameters['gatk_base_recalibrator'] : ''
      gatk_apply_bqsr_parameters = base_recalibrator_tool_parameters['gatk_apply_bqsr'] ? base_recalibrator_tool_parameters['gatk_apply_bqsr'] : ''
      bams_to_base_qual_recal_w_indices(
        alns,
        known_sites_ref,
        make_ancillary_index_files.out.collective_idx_files,
        '',
        gatk_index_feature_file_parameters,
        bed,
        gatk_base_recalibrator_parameters,
        gatk_apply_bqsr_parameters)
      // This truncating step assumes ABRA2 was run upstream. May be best to
      // have this outside the base requal conditional and instead perform it
      // before emission assuming ABRA2 was called.
      alns
        .map{ [it[0], it[1].split('-rel-')[0], it[2], it[3]] }
        .set{ trunc_alns2 }
      bams_to_base_qual_recal_w_indices.out.bams
        .map{ [it[0], it[1].split('-rel-')[0], it[2], it[3]] }
        .set{ trunc_base_recal_bams }
      trunc_alns2
        .join(trunc_base_recal_bams, by: [0, 1, 2])
        .flatten()
        .filter{ it =~ /abra.bam$|sorted.bam$|mkdup.bam$/ }
        .set { init_bams_done_signal3 }
      clean_init_bams3(
        init_bams_done_signal3)
      trunc_base_recal_bams
        .set{ alns }
    }
    samtools_index_procd(
      alns,
      '')
  emit:
    procd_bams_and_bais = samtools_index_procd.out.bams_and_bais
    procd_bams = alns
//    procd_bais = samtools_index.out.bais
    anc_idx_files = make_ancillary_index_files.out.collective_idx_files
    marked_dup_metrics
}

workflow make_ancillary_index_files {
  take:
    ref
  main:
    samtools_faidx(
      ref,
      '')
    picard_create_seq_dict(
      ref,
      '')
    //Can't join since the ref symlinks are different.
    samtools_faidx.out.faidx_file
      .concat(picard_create_seq_dict.out.dict_file)
      .collect().map{ [it[0], it[1], it[3]] }
      .set{ collective_idx_files }
  emit:
    collective_idx_files
}

workflow bams_to_base_qual_recal_w_indices {
  take:
    alns
    known_sites
    collective_idx_files
    samtools_index_parameters
    gatk_index_feature_file_parameters
    targets_bed
    gatk_base_recalibrator_parameters
    gatk_apply_bqsr_parameters
  main:
    samtools_index_recal(
      alns,
      samtools_index_parameters)
    gatk_index_feature_file(
      known_sites,
      gatk_index_feature_file_parameters)
    gatk_base_recalibrator(
      samtools_index_recal.out.bams_and_bais,
      collective_idx_files,
      gatk_index_feature_file.out.ff_w_index,
      targets_bed,
      gatk_base_recalibrator_parameters)
    gatk_apply_bqsr(
      gatk_base_recalibrator.out.grps,
      collective_idx_files,
      targets_bed,
      gatk_apply_bqsr_parameters)
    samtools_index_bqsr(
      gatk_apply_bqsr.out.bams,
      samtools_index_parameters)
  emit:
    bams_and_bais = samtools_index_bqsr.out.bams_and_bais
    bams = gatk_apply_bqsr.out.bams
    bais = samtools_index_bqsr.out.bais
}
